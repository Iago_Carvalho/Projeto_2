----------------------------------------------------------------------------------------------------
README
Orientação a Objetos 
Exercício de programação 2
07/06/2016
Iago Vasconcelos de Carvalho
-----------------------------------------------------------------------------------------------------

**Recomenda-se a leitura no modo "raw".

-----------------------------------------------------------------------------------------------------
I. INFORMAÇÕES GERAIS
-----------------------------------------------------------------------------------------------------
  -O software foi criado usando o OS Ubuntu 14.04 LTS.
  -É de extrema importância que o usuário utilize uma distribuição linux para o uso correto
   desse software.
  -Recomenda-se a utilização de uma distribuição 64-bit para a prevenção de erros.
  -É de extrema importância que o usuário utilize a IDE Eclipse para a utilização do programa.
  -Recomenda-se a utilização da versão Mars 2 4.5.2 do Eclipse.
------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------
II. COMO USAR O PROGRAMA
------------------------------------------------------------------------------------------------------
   -O pacote interface contém a interface que seria implementada inicialmente mas que não será
   utilizada, apesar de estar funcional sem integração do código.
------------------------------------------------------------------------------------------------------  
PGM
------------------------------------------------------------------------------------------------------
   -Por padrão o programa está configurado para ser utilizado na imagem pgm.
   -O usuário deverá "iniciar"(compilar) o arquivo Main.JAVA na pasta src do projeto.
   -O usuário deverá escolher o arquivo .pgm na interface.
   -A mensagem será decifrada e os filtros serão aplicados.
   -***A mensagem será aberta em uma janela separada da imagem com filtro. 
   -***Caso as outras imagens (filtradas) não apareçam na tela verifique o tamanho da janela e tente 
    redimensioná-la. 
-------------------------------------------------------------------------------------------------------
PPM
-------------------------------------------------------------------------------------------------------
  -Todos os filtros da ppm são funcionais e aplicáveis e serão mostrados na interface, porém devido a 
   complicações no desenvolvimento da interface do código o usuário deverá retirar os comentários das 
   funções de leitura e aplicação de filtro na ppm (linha 42 até 45) e comentar as funções pertencentes 
   à pgm (linha 49 até 58).  
-------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------
III. INTRUÇÕES PARA COMPILAÇÃO DO PROGRAMA
-------------------------------------------------------------------------------------------------------
  -Para compilar o código o usuário deve fazer o download do mesmo pela plataforma gitlab ou 
   utilizar o comando " git clone git@gitlab.com:Iago_Carvalho/Projeto_2.git ".
  -Abra o projeto na IDE Eclipse.
  -Use o "botão" run no arquivo main localizado no pacote default na pasta src .
--------------------------------------------------------------------------------------------------------
