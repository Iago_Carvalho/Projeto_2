import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ConvolveOp;
import java.awt.image.DataBufferByte;
import java.awt.image.Kernel;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Scanner;

public class pgm extends ImageDecipher {
	
//Atributes  
	int Initial_Position = 0;
    String Message;
	char Extracted_Bit = 0;
	char FileByte = 0;

//Receive initial position
	public void setInitialPos(int initialPos) {
		this.Initial_Position = initialPos;
	}
	public int getInitial_Position(File file) {
		return Initial_Position;
	}
//Receive path	
		pgm(String path){
			this.setPath(path);
		}

//Find Hidden Message
	@Override
	public String Hidden_Message(String filePath) throws FileNotFoundException, IOException {
		File file = new File(filePath);
		RandomAccessFile openedFile = null;
        openedFile = new RandomAccessFile(path, "r");
        openedFile.seek(ImageBegining(filePath) + HiddenMessage_Begining(filePath));
        
        char generatedCharacter = 0x00;
        String HiddenMessage;
        int Read_Bit;
        
        for (int i=1; i<=8; i++){
        	Read_Bit = openedFile.read() & 0x01;
        	generatedCharacter = (char)((generatedCharacter << 1) | Read_Bit);
        }
        HiddenMessage = Character.toString(generatedCharacter);
        while (true) {
            generatedCharacter = 0x00;
            for (int i = 1; i <= 8; i++) {
                Read_Bit = openedFile.read() & 0x01;
				generatedCharacter = (char) ((generatedCharacter << 1) | Read_Bit);
		}
            if (generatedCharacter == '#')
				break;
            HiddenMessage += Character.toString(generatedCharacter);
        }
        return HiddenMessage;
}
	
//Hidden Message Begining
	private static int HiddenMessage_Begining(String filePath) throws IOException {

		File file = new File(filePath);
		RandomAccessFile openedFile = null;
		openedFile = new RandomAccessFile(file, "r");

		char generatedCharacter;
		char spaceChar = ' ';
		String messageBegin = "";

		while (true) {
             generatedCharacter = (char) openedFile.read();

	    if (generatedCharacter == '#') {
             openedFile.read();

		while (true) {
			generatedCharacter = (char) openedFile.read();
		if (generatedCharacter != spaceChar)
			messageBegin = messageBegin + generatedCharacter;
					else
						break;
				}
				break;
			}
       }
        openedFile.close();
		return Integer.parseInt(messageBegin);
 }

//Read PGM Image
	public BufferedImage readImage(){
	   try {
		FileInputStream file = new FileInputStream(getPath());
			int count = 0;
			byte bt;
			String line;
			
			line = pgm.readLine(file);
		if("P5".equals(line)){
				line = pgm.readLine(file);
				
		while(line.startsWith("#")){
					line = pgm.readLine(file);
				}
				Scanner sizes = new Scanner (line);
				if(sizes.hasNext() && sizes.hasNextInt()){
					width = sizes.nextInt();
				}
	    if(sizes.hasNext() && sizes.hasNextInt()){
					height = sizes.nextInt();
				}
				sizes.close();
				line = pgm.readLine(file);
				sizes = new Scanner(line);
				Max_Color = sizes.nextInt();
				
				sizes.close();
				
				imageIn = new BufferedImage(width, height, BufferedImage.TYPE_BYTE_GRAY);
				Pixels = ((DataBufferByte) imageIn.getRaster().getDataBuffer()).getData();
				while(count < (width * height)){
					bt = (byte) file.read();
					Pixels[count] = bt;
					count++;
				}
	    }
} catch (Throwable e) {
			e.printStackTrace();
		}
		return imageIn;
	}

//Blur filter
	public BufferedImage blurFilter(){
			
			BufferedImage filteredPgmBlur = new BufferedImage(width,height,BufferedImage.TYPE_BYTE_GRAY);
			float[] matrix = new float[9];
			for (int i = 0; i < 9; i++)
				matrix[i] = 1.0f/9.0f;
	            BufferedImageOp op = new ConvolveOp( new Kernel(3, 3, matrix), ConvolveOp.EDGE_ZERO_FILL, null );
		    	Object blurredImage = op.filter(imageIn, filteredPgmBlur);
			
		    	return filteredPgmBlur;
	   }
//Sharpen filter
	public BufferedImage sharpenFilter(){
		BufferedImage filteredPgmSharpen = new BufferedImage(width,height,BufferedImage.TYPE_BYTE_GRAY);
		float[] matrix = {0,-1,0,-1,5,-1,0,-1,0};
		BufferedImageOp op = new ConvolveOp( new Kernel(3, 3, matrix), ConvolveOp.EDGE_ZERO_FILL, null );
    	Object blurredImage = op.filter(imageIn, filteredPgmSharpen);
    	
    	return filteredPgmSharpen;
	   }
	
//Negative filter
	public BufferedImage negativeFilter(){
		byte bt;
		int counter = 0;
		BufferedImage filteredPgmNegative = new BufferedImage(width,height,BufferedImage.TYPE_BYTE_GRAY);
		byte [] pixelsNegative = ((DataBufferByte) filteredPgmNegative.getRaster().getDataBuffer()).getData(); 
		while(counter < (width * height)){
			bt = Pixels[counter];
			pixelsNegative[counter] = (byte)(Max_Color - bt);
			counter++;
		}
		return filteredPgmNegative;
	}
}
