package Interface;

import java.awt.EventQueue;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.filechooser.FileNameExtensionFilter;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;

import javax.swing.JMenu;

public class Tela_principal extends JFrame {

	private static final long serialVersionUID = 1L;
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Tela_principal frame = new Tela_principal();
					frame.setVisible(true);
					frame.setTitle("EP2 OO 2016.1");
					frame.getContentPane().setBackground(Color.LIGHT_GRAY);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public Tela_principal() {
		setSize(640, 480);
//Main menu		
		JMenuBar menuBar = new JMenuBar();		
		setJMenuBar(menuBar);

//File Choose		
		JFileChooser chooser = new JFileChooser();
		File workingDirectory = new File(System.getProperty("user.dir"));
		chooser.setCurrentDirectory(workingDirectory);
		int returnVal = chooser.showOpenDialog(getParent());
		if(returnVal == JFileChooser.APPROVE_OPTION) {
		   System.out.println("You chose to open this file: " +
		        chooser.getSelectedFile().getName());
		}
		
//Hidden message menu        
        JMenu mnMessage = new JMenu("Decipher Image");					
		menuBar.add(mnMessage);
		JMenuItem mntmSolvePgm = new JMenuItem("Find Hidden message");
		mnMessage.add(mntmSolvePgm);
		mntmSolvePgm.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				@SuppressWarnings("unused")
				Hidden_Message decipherFrame = new Hidden_Message();
			}
		});

//Filter's Menu		
		JMenu mnApply = new JMenu("Apply Filter");				
		menuBar.add(mnApply);
		JMenuItem mntmToPgm = new JMenuItem("PGM");
		mnApply.add(mntmToPgm);
		JMenuItem mntmToPpm = new JMenuItem("PPM");
		mnApply.add(mntmToPpm);
        mntmToPpm.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				@SuppressWarnings("unused")
				Apply_Filter ppmFilterScreen = new Apply_Filter();
			}
		});

//Info Menu
		JMenu mnInfo = new JMenu("Info"); 							
		menuBar.add(mnInfo);
		JMenuItem mntmInfo = new JMenuItem("About");
		mnInfo.add(mntmInfo);
		mntmInfo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String message = "Creator..."
						+ '\n'+"Version...";
				JOptionPane.showMessageDialog(null, message);
			}
		});
		
   }
}


