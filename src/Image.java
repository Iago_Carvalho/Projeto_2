import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

abstract class ImageDecipher {
	
	protected int width;
	protected int height;
	protected String Magic_Number;
	protected int Max_Color;
	protected String path;
	protected char[] pixels;
	protected byte[] Pixels = null;
	protected char[] filtered_Image;
	protected char[] filteredPPM;
	protected BufferedImage imageIn = null;
	
	public BufferedImage readImage(){
		return null;
	}
	public int getWidth() {
		return width;
	}
	public int getHeight() {
		return height;
	}
	public String getMagicNumber() {
		return Magic_Number;
	}
	public int getMaxColorNumber() {
		return Max_Color;
	}
    public void setPath(String path) {
		this.path = path;
	}
	public String getPath() {
		return path;
	}


//Read ppm line
	public static String readLine(FileInputStream file){
		String line= "";
		byte bb;
		try {
			while((bb = (byte) file.read()) != '\n'){
				line += (char) bb; 
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return line;
	}
//Receive Height
		public void get_Height (String path) throws FileNotFoundException, IOException {
			File file = new File(path);
			RandomAccessFile openedFile = null;
			openedFile = new RandomAccessFile(file, "r");
			String giveHeight = null;
			char read_Character;
			int broken_Lines = 0;
			
			while(true){
			read_Character = (char) openedFile.read();
			     if (read_Character == '\n')
			      broken_Lines ++;
			     
			     if(broken_Lines == 2)
			    	 break;
			}
			     while(true){
			    read_Character = (char) openedFile.readInt();
			    
			    if(read_Character =='\n')
			    	break;
			    else
			    	giveHeight += read_Character; 
			     }
			     openedFile.close();
					this.height = Integer.parseInt(giveHeight);
					
			   }
//Recebe Width
		public void get_Width (String path) throws FileNotFoundException, IOException {
			
			File file = new File(path);
			RandomAccessFile openedFile = null;
			openedFile = new RandomAccessFile(file, "r");
			
			String giveHeight = null;
			char read_Character;
			int broken_Lines = 0;
			
     		while(true){
				read_Character = (char) openedFile.read();
					
				if(read_Character == '\n')
						broken_Lines ++;
			
				if(broken_Lines == 2)
					break;
			}
			while(true){
				read_Character = (char) openedFile.readInt();
				
				if(read_Character == '\n')
				break;
				else
					giveHeight += read_Character;
				}
			openedFile.close();
		this.width = Integer.parseInt(giveHeight);
		}
//Receive Magic number
		public void get_MagicNumber (String path) throws FileNotFoundException, IOException {
			
			File file = new File(path);
			RandomAccessFile openedFile = null;
			openedFile = new RandomAccessFile(file, "r");
		
		    String MagicNumber;
		    int read_Character;
		    
		    read_Character = openedFile.read();
		    MagicNumber = Character.toString((char)read_Character);
		    read_Character = openedFile.read();
		    MagicNumber += Character.toString((char)read_Character);
		    
		    this.Magic_Number = MagicNumber; 
		}

//Image Begining
protected static long ImageBegining(String path) throws FileNotFoundException, IOException{
	
	        File file = new File(path);
	        RandomAccessFile openedFile = null;
	        openedFile = new RandomAccessFile(path, "r");
	
	        int broken_Lines = 0;
	
	        while(true){
		    if(openedFile.read() == '\n')
		       broken_Lines ++;
		    if(broken_Lines == 4)
			   break;
	     }
	        long cursorPosition = openedFile.getFilePointer();
	        openedFile.close();
	
	        return cursorPosition;
	}
//Open image
      public BufferedImage openImage() {
        BufferedImage image = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_BYTE_GRAY);
        int counter = 0;
        char[] pixelsImage = get_Pixels();
        byte[] pixels;
        pixels = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
        while (counter < getHeight() * getWidth()) {
          pixels[counter] = (byte) pixelsImage[counter];
          counter++;
    }
    return image;
}
//Open filtered image
      public BufferedImage openImage(char[] filteredImage) {
        BufferedImage image = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_BYTE_GRAY);
        int counter = 0;
        byte[] pixels;
        pixels = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
        while (counter < getHeight() * getWidth()) {
          pixels[counter] = (byte) filteredImage[counter];
          counter++;
    }
    return image;
}
//Receive pixels char[]
       public void set_Pixels (String Path) throws IOException{
		
    	File file = new File(path);
		RandomAccessFile openedFile = null;
		openedFile = new RandomAccessFile(path, "r");
		openedFile.seek(ImageBegining(path));
		char[] pixels = new char[getHeight() * getWidth()];
		
		for (int i = 0; i < pixels.length; i++) {
			pixels[i] = (char)openedFile.read();
		}
        openedFile.close();
        setPixels(pixels);
	    }
      public void setPixels(char[] pixels){
         this.pixels = pixels;
      }
      public char [] get_Pixels(){
   	 return pixels;
 }
      


//Hidden message
abstract String Hidden_Message(String filePath) throws FileNotFoundException, IOException;
}

